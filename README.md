[Documentation](https://bitbucket.org/lupuionut/easyappointment/wiki/Home) | [Download](https://bitbucket.org/lupuionut/easyappointment/downloads/)

# Changelog
### 2.5.2 (September 2021)
* New. You can now create a custom field of type "checkbox" to appear on the reservation form page. This can be useful if you want to enforce the user to agree with terms and conditions.
* Changed. In the management area, on the bookings list page, the email and the phone are now combined to appear in the same column.
* Fixed. If a custom field was used in the reservation form, on the bookings list page, the comments field was not properly showing the value of the custom field saved as a comment.

### 2.5.1 (August 2021)
* New. When you create or edit a service, in the admin area, you can select an attachment (pdf file) for a certain service. In the front-end, in the management area, you can select to send that attachment in the confirmation email to the client.
* Changed. Email, phone, comments area now visible on the page containing the list of bookings in the management area.

### 2.5.0 (July 2021)
* New. Introduce option to set a service as gapless. Gapless means that this service allows choosing a single time slot at a time. Only after choosing a time slot, the next time slot will become available.
* New. Introduce option to set a limit of "n" days for a service availability, so the client can make a reservation only "n" days from the current day.
* Changed. Intervals that are part of a daily schedule, can be dedicated to all services provided by a staff member or to a single service.
* Changed. Service length includes also values of 1,2,3,4 minutes.

### 2.4.17 (July 2021)
* Fixed. When setting the schedule for the first time, adding more than one interval was not properly handled, leaving the false impression that the interval was not added.

### 2.4.16 (July 2021)
* Fixed. A bug was present when changing the Joomla user assigned to a staff member.

### 2.4.15 (April 2020)
* Changed. New update url.

### 2.4.14 (July 2019)
* Changed. Starting this month, EasyAppointment is available free of charge. That includes also future updates. For this reason, I've removed the update token and added a support button on the about page, so that you can support me via PayPal to continue working on this extension.

### 2.4.13 (June 2019)
* New. Added support for recaptcha plugin. You must enable the default recaptcha plugin for Joomla and this option and your reservation form will use it.

### 2.4.12 (April 2019)
* Fixed. Email validation in staff management area.

### 2.4.11 (April 2019)
* Fixed. Email validation during the reservation process was not correct on servers with newer php versions.

### 2.4.10 (November 2018)
* Fixed. A bug introduced in 2.4.9 (staff members could not save details of an already existing reservation). 

### 2.4.9 (November 2018)
* Fixed. Staff members could book already reserved slot.
* Changed. On manager booking page, all error messages are now persistent, they do not hide automatically.

### 2.4.8 (May 2018)
* New. Add support to receive notification via email when client cancels his appointment
* Changed. Add new values to default timeframe/calendar

### 2.4.7
* New. Option to disable sef url.

### 2.4.6
* New. Calendar timeframe. You can specify the time length between two slots in calendar. Fix for Issue #6

### 2.4.5
* New. Two new tags, address and staff name. Issue #2
* Fixed. Non existing time slots in the calendar view. Issue #11
* Fixed. Error 500 when trying to confirm a reservation in the history area. Issue #12  
* Fixed. Incorrect number of reservations in the management area. Issue #13

### 2.4.4
* New. Order reservations in management area. Issue #8
* Changed. More currencies to select. Issue #9
* Fixed. Wrong order of services in the management area.  Issue #3  
* Fixed. Wrong display of custom fields in confirmation email. Issue #1

### 2.4.3
* Fixed. Warning on information page when no custom field defined.

### 2.4.2
* Changed. Unique keyring to identify a reservation is generated using php uniqid 
* Fixed. When saving staff preferences, defined custom fields were deleted

### 2.4.1
* Fixed. Error for older versions of php. See https://ionutlupu.me/discussions/topic/read.html?pid=315
* Fixed. On some servers, emails not sent. It should use only email's "from" configuration value.

### 2.4
* New. Add new column in the reservation history table. The new field shows the staff corresponding to the reservation.
* New. Staff can choose if to display or not his specializations on the page with the list of staff members available for a service.
* New. Staff can choose if a canceled reservation should be marked as unconfirmed or deleted.
* New. Implemented a checkbox on the reservation page, in the management area. When this option is checked, client receives a notification via email about the new reservation.
* New. Ordering services. You can choose the order of your services from back-end.
* New. Filter by email/name in the management area (front-end), on the reservations list page.
* New. A manager or more can be defined in the back-end Options. This can manage all reservations from all staff members in the management front-end area.
* Changed. A custom field can be defined as mandatory or not.

### 2.3.1
* New. Set the status of an unconfirmed reservation timeslot. When the status of a reservation was unconfirmed, the timeslot allocated to that reservation was not available. You can now choose if in this situation that timeslot is available.
* Changed. Confirmation/cancelation link includes only keyring.
* Fixed. Wrong values for the service length and price.
* Fixed. The service length was not properly displayed on the calendar page.
* Fixed.  Switched "Next week" with "Previous week" and "Previous week" with "Next week" 

### 2.3
* New. Order font-end staff list by name or id.
* New.  Staff member can choose to only allow appointments to be done "+ x days" from current date.
* New.  Creation of custom fields (text input or text area) for appointment form. The values of these fields will be stored as appointment comment.
* New. Implemented a reminder (a notification by email few days before the appointment date). In order for this to function, you must set a daily cron job to run the following file: <joomla root path>/components/com_easyappointment/lib/cron.php
* New.  Implemented the update system for the component. To enable the auto update you must add the "update token" in the "About" section from back-end. This token can be obtained from https://ionutlupu.me/your-desk/orders.html
* Changed. Values of service length in back and front-end management area.
* Changed. Improved SEO. In case of services or staff page, the title will reflect the name of service/staff member; modified the url to include staff and service name.

### 2.2
* New. Option to remove "Specializations" from staff profile page.
* New. Option to send email notifications in HTML format.
* New.  "service" tag into the notification.
* Changed. Better support for calendar view in mobile devices. In the previous versions, the table containing the calendar doesn't fit the display size of a mobile phone.
* Changed. Modified validation system for reservation form's required fields to prevent possible conflicts with other modules with required fields on the same page.
* Changed. Modified calendar navigation. The user will see "Previous week" and "Next week" only if that staff member has set his calendar to display more than 1 week.

### 2.1.1
* Fixed. In the management booking edit/view, the staff could not correctly choose ending hour.

### 2.1
* New. implemented option to restrict reservations only to registered users
* New. reservations history area, where each user can verify his past reservations. This will work only for reservations from registered users.
* New. a staff list page in the front-end
* New. new export format: ICS
* New. new date format
* Fixed. fixed a bug in selection of date (UTC)